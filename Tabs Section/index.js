const tabs = document.querySelector(".tabs");
const btns = document.querySelectorAll(".Button");
const articles = document.querySelectorAll(".content");

tabs.addEventListener("click", (event)=>{
// console.log(e.target.dataset.id); //to check which button is clicked by user
const id = event.target.dataset.id;

if(id){
    btns.forEach((btn)=>{
        btn.classList.remove("live");
    })
    event.target.classList.add("live");
    articles.forEach((article)=>{
         article.classList.remove("live");
    })
    const element = document.getElementById(id); //we have saved data-id of all steps in id variable so we get id and add live class.
    element.classList.add("live");
}
}); 