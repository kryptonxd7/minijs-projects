const btnEl = document.querySelector(".btn");
const closeIconEl =document.querySelector(".close-icon");
const trailerContainerEl = document.querySelector(".trailer-container");
const videoEl = document.querySelector("video");
btnEl.addEventListener("click", ()=>{
    trailerContainerEl.classList.remove("active"); // when active removed video pop ups
})

closeIconEl.addEventListener("click", ()=>{
    trailerContainerEl.classList.add("active"); // when active added video visibility: hidden;
    videoEl.pause(); // video paused
    videoEl.currentTime = 0; //sets the time to 0 
})