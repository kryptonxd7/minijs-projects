//argument vs parameters
function greetUsers(name, greeting) { //parameters: created inside of the function
    console.log(`${greeting}${name}`);
}
greetUsers("Welcome","sujan");
 //arguments: They are created outside of the function
let firstNum = 3;
let secondNum = 4;
 function Sum(num1, num2) {
    //params
    return num1 + num2;
 }
 Sum(firstNum, secondNum); //arguments


 //array as parameters
let array = [1, 2, 3, 4];
 function  getFirst(arr) {
    return arr[0];
 }

let result = getFirst(array);
 console.log(result);
