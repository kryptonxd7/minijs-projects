let myLeads = [];
// let because values can be reassigned
const inputEl = document.getElementById("input-el"); // const because values cannot be reassigned
const inputBtnEl = document.getElementById("input-button");
const ulEl = document.getElementById("ul-el");
const deleteBtn = document.getElementById("delete-button");
const saveTabBtn = document.getElementById("save-tab");
const leadsFromLocalStorage = JSON.parse(localStorage.getItem("myLeads")); //parsing data stored in string format to array.
if (leadsFromLocalStorage) {
  myLeads = leadsFromLocalStorage;
  render(myLeads);
}
inputBtnEl.addEventListener("click", function () {
  myLeads.push(inputEl.value); //push the data taken from users
  inputEl.value = ""; //clear out
  localStorage.setItem("myLeads", JSON.stringify(myLeads)); //converting array to String.
  //localstorage only stores data in string format
  render(myLeads);
});
deleteBtn.addEventListener("dblclick", function () {
  localStorage.clear();
  myLeads = [];
  render(myLeads);
});
saveTabBtn.addEventListener("click", function () {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    myLeads.push(tabs[0].url);
    localStorage.setItem("myLeads", JSON.stringify(myLeads));
    render(myLeads);
  });
});
function render(leads) {
  let listItems = "";
  for (let i = 0; i < leads.length; i++) {
    listItems += `<li>
    <a href='${leads[i]}' target ='_blank'>
    ${leads[i]}
    </a>
    </li>`;
    //different method
    //create element
    // set text content
    //append to ul
    //   const li = document.createElement("li");
    //   li.textContent = myLeads[i];
    //   ulEl.append(li);
  }
  ulEl.innerHTML = listItems; //innerHTML: browser look at string and think it as list element.
}
