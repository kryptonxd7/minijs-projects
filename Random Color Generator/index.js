const containerEl = document.querySelector(".container"); // querySelector returns the first element and matches the CSS selector
//get the first element with class = "container";
for (let index = 0; index < 30; index++){ 
    const colorContainerEl = document.createElement("div");//creates  30 div element
    colorContainerEl.classList.add("color-container"); // adds the class name color-container
    containerEl.appendChild(colorContainerEl);  //appends a element colorContainer as the last child of containerEl.
}

const colorContainerEls = document.querySelectorAll(".color-container"); //return all matches not only first.
//gets all elements with class = "color-container";
generateColors();

function generateColors(){ 
colorContainerEls.forEach((colorContainerEl)=>{ //for each colorContainerEl newColorCode is generated which calls function randomColor
    const  newColorCode = randomColor();
    colorContainerEl.style.backgroundColor = "#" + newColorCode;
    colorContainerEl.innerText = "#" + newColorCode;
})
}
function randomColor(){
    const chars = "0123456789abdcdef";
    const colorCodeLength = 6;
    let colorCode = "";
    for (let index = 0; index < colorCodeLength; index++) {
        const randomNumber = Math.floor(Math.random() * chars.length);
        colorCode += chars.substring(randomNumber, randomNumber + 1); 
    }
    return colorCode;
}